#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QMessageBox>

#include "proc.h"

#include <fstream>
#include <string>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer = new QTimer(this);

    setWindowTitle("Conta Coso");

    connect(timer,SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);


    chart->setAnimationOptions(QChart::SeriesAnimations);
    chart->legend()->hide();
    chart->addSeries(series);


    chart->createDefaultAxes();


    chart->axisX()->setRange(0, 1);
    chart->axisY()->setRange(0, 100);


    chart->setTitle("CPU usage");


    ui->chartView->setChart(chart);
    ui->chartView->setRenderHint(QPainter::Antialiasing);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setPid(pid_t pid)
{
    ui->pidEdit->setText(QString::number(pid));
}


void MainWindow::update()
{
    static long unsigned rchar_old=0, wchar_old=0;
    static float cputime_old=0;


    float cputime;

    std::string pids=ui->pidEdit->text().toUtf8().constData();

    if(!std::any_of(pids.begin(), pids.end(), ::isdigit))
        return;

    pid_t pid = std::stoi(pids);

    ui->statusBar->showMessage(QString("Attached process: ")+Proc::cmdline_from_pid(pid).c_str());



    size_t rchar, wchar;
    std::tie(rchar, wchar) = Proc::getDiskRW(pid);

    int tickrate=Proc::getTickRate();

    cputime=Proc::getProcessTicks(pid)/float(tickrate);


    if(rchar_old==0)
        goto skip_first;

    ui->wchar->setText(QString::number(wchar/1024/1024)+" MiB");
    ui->rchar->setText(QString::number(rchar/1024/1024)+" MiB");


    ui->wchar_now->setText(QString::number((wchar - wchar_old)/1024)+" KiB/s");
    ui->rchar_now->setText(QString::number((rchar - rchar_old)/1024)+" KiB/s");

    ui->cpu_total->setText(QString::number( cputime ) + " s");
    ui->cpu_now->setText(QString::number( (cputime - cputime_old)*100.0 , 'G',2 ) + " %");



    series->append(t_counter, (cputime - cputime_old)*100.0 );


    chart->axisX()->setRange(0, t_counter);

    t_counter++;


    skip_first:

    rchar_old=rchar;
    wchar_old=wchar;
    cputime_old=cputime;

}
