#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QtDebug>

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>

QT_CHARTS_USE_NAMESPACE


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setPid(pid_t pid);

public slots:
    void update();

private:
    Ui::MainWindow *ui;
    QTimer* timer;
    QLineSeries *series = new QLineSeries();
    QChart *chart = new QChart();

    int t_counter=0;

};

#endif // MAINWINDOW_H
