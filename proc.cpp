#include "proc.h"

Proc::Proc()
{


}


unsigned long Proc::getProcessTicks(pid_t pid){
    std::ifstream infile("/proc/"+std::to_string(pid)+"/stat");
    if(infile.fail()){
        return 0;
    }
    std::string line;
    std::getline(infile, line);

    std::istringstream iss(line);
    std::vector<std::string> results((std::istream_iterator<std::string>(iss)),
    std::istream_iterator<std::string>());

    unsigned long long utime = std::stoul(results[13]);
    unsigned long long stime = std::stoul(results[14]);

    return utime + stime;
}

int Proc::getTickRate()
{
    return sysconf(_SC_CLK_TCK);
}


std::string Proc::cmdline_from_pid(pid_t pid){
    std::ifstream infile("/proc/"+std::to_string(pid)+"/cmdline");
    if(infile.fail()){
        return "";
    }
    std::string line;
    std::getline(infile, line);

    return line;

}


std::tuple<size_t, size_t> Proc::getDiskRW(pid_t pid)
{
    std::string a, b;
    size_t rchar, wchar;
    float cputime;



    std::ifstream infile("/proc/"+std::to_string(pid)+"/io");
    if(infile.fail()){
        return std::tuple<int,int> {0,0};
    }



    while (infile >> a >> b)
    {
        if(a=="rchar:")
            rchar=std::stoul(b);

        if(a=="wchar:")
            wchar=std::stoul(b);
    }

    return std::make_tuple(rchar, wchar);

}
