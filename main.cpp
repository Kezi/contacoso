#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    if(argc>1){
        w.setPid(std::stoul(argv[1]));
    }

    return a.exec();
}
