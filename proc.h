#ifndef PROCPIDSTAT_H
#define PROCPIDSTAT_H
#include <sys/types.h>
#include <QString>

#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <sstream>

#include <unistd.h>
#include <tuple>

class Proc
{
public:
    Proc();

    static unsigned long getProcessTicks(pid_t pid);

    static int getTickRate();
    static std::string cmdline_from_pid(pid_t pid);

    static std::tuple<size_t,size_t> getDiskRW(pid_t pid);




};

#endif // PROCPIDSTAT_H
